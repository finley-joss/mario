package platformer.controller;





import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.net.URL;

public class Music  {


    URL resource = getClass().getResource("/platformer/music/bg.wav");
    MediaPlayer bgmusic =new MediaPlayer(new Media(resource.toString()));
    private boolean ismute = false;
    public void music()

    {
        bgmusic.setOnEndOfMedia(new Runnable() {
            public void run() {
                bgmusic.seek(Duration.ZERO);
            }
        });

        bgmusic.play();

    }
    public void mutebg(){
        //music();
        bgmusic.setVolume(0);
    }
    public void unmutebg(){
        //music();

        bgmusic.setVolume(100);
    }
    public void getsoundmute(){
        soundjump.stop();
        soundtp.stop();
    }
    URL resource1 = getClass().getResource("/platformer/music/jump.wav");
    AudioClip soundjump = new AudioClip(resource1.toString());
    URL resource2 = getClass().getResource("/platformer/music/tp.wav");
    AudioClip soundtp = new AudioClip(resource2.toString());
    public void getSoundjump() {
        soundjump.play();
    }

    public void getSoundtp() {
        soundtp.play();
    }

    public Boolean ismute(){return ismute;}
}

