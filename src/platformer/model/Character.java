package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import platformer.controller.Music;
import platformer.view.Platform;

import java.util.concurrent.TimeUnit;

public class Character extends Pane {

    public static final int CHARACTER_WIDTH = 32;
    public static final int CHARACTER_HEIGHT = 64;

    private Image characterImg;


    private AnimatedSprite imageView;

    private int x;
    private int y;

    private int score=0;
    private int scoreball = 0;

    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;
    private KeyCode dashKey;

    int xVelocity = 0;
    int yVelocity = 0;
    int xAcceleration = 1;
    int yAcceleration = 1;
    int xMaxVelocity = 7;
    int yMaxVelocity = 17;
    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean falling = true;
    boolean canJump = false;
    boolean jumping = false;
    boolean dashing = false;


    private Music music = new Music();
    private KeyCode muteKey;
    private KeyCode unmuteKey;
    boolean soundmute = false;
    boolean soundbgmute = false;


    private int startX;
    private int startY;
    private int offsetX;
    private int offsetY;
    private int stamina;

    private int jumpCount;
    public Character(int x, int y, int offsetX, int offsetY, KeyCode leftKey, KeyCode rightKey, KeyCode upKey,KeyCode dashKey) {
        this.jumpCount=0;
        this.startX = x;
        this.startY = y;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.stamina = 125;

        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.characterImg = new Image(getClass().getResourceAsStream("/platformer/assets/MarioSheet.png"));
        this.imageView = new AnimatedSprite(characterImg,4,4,offsetX,offsetY,16,32);
        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.upKey = upKey;
        this.dashKey = dashKey;
        this.getChildren().addAll(this.imageView);
    }
    public int getBall() {
        return scoreball;
    }

    public Character(KeyCode mutekey,KeyCode unmutekey){
        this.muteKey=mutekey;
        this.unmuteKey=unmutekey;
    }

    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }
    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }
    public void hit(Ball c) throws InterruptedException {
        scoreball++;
        c.repaint();
        c.delete();

    }


    public void stop() {
        isMoveLeft = false;
        isMoveRight = false;
        xVelocity = 0;
    }

    public void jump() {
        if (canJump && stamina>=40) {
            yVelocity = yMaxVelocity;
            if(jumpCount>=1) {
            	canJump = false;
            }
            jumping = true;
            falling = false;
            stamina-=40;
            jumpCount++;
        }
    }

    public void jumpsound(){
    	if(canJump && stamina>=40) {
    		music.getSoundjump();
    	}
        
        //isplayjumpsoumd = true;

    }
    public void warpsound(){
    	if(dashing) {
        music.getSoundtp();
    	}//isplaywarpsound = true;
    }
    public void mute(){
        //System.out.println("pass");
        music.getsoundmute();

        soundmute= true;
        soundbgmute = true;
    }
    public void unmute(){

        soundmute = false;
        soundbgmute = false;
    }
    
    public void dash() {
    	if(stamina>=40) {
    		dashing = true;
    		stamina-=40;
    	}
    }

    public void checkReachHighest() {
        if(jumping &&  yVelocity <= 0) {
            jumping = false;
            falling = true;
            yVelocity = 0;
        }
    }

    public void checkReachFloor() {
        if(falling && y >= Platform.GROUND - CHARACTER_HEIGHT) {
 		    y = Platform.GROUND - CHARACTER_HEIGHT;
            falling = false;
            canJump = true;
            yVelocity = 0;
            jumpCount = 0;
        }
    }

    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-CHARACTER_WIDTH;
        }
    }

    public void checkMute(){

        if (soundmute==true){
            mute();
        }else {
            unmute();
        }
    }

    public void moveX() {
        setTranslateX(x);

        if(isMoveLeft) {
        	if(dashing) {
           		xVelocity = 140;
           		dashing = false;
           		dashAnimate();
        	}else {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
        	}            
        	x = x - xVelocity;
        }
        if(isMoveRight) {
        	if(dashing) {
           		xVelocity = 140;
           		dashing = false;
           		dashAnimate();
        	}else {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
        	}
            x = x + xVelocity;
        }
    }

    public void moveY() {
        setTranslateY(y);

        if(falling) {
            yVelocity = yVelocity >= yMaxVelocity? yMaxVelocity : yVelocity+yAcceleration;
            y = y + yVelocity;
        }
        else if(jumping) {
            yVelocity = yVelocity <= 0 ? 0 : yVelocity-yAcceleration;
            y = y - yVelocity;
        }
    }

    public void repaint() {
        moveX();
        moveY();
    }

    public void collided(Character c) {
            if (isMoveLeft) {
                x = c.getX() + CHARACTER_WIDTH + 1;
                stop();
            } else if (isMoveRight) {
                x = c.getX() - CHARACTER_WIDTH - 1;
                stop();
            }

            //Not on the ground
            if(y < Platform.GROUND - CHARACTER_HEIGHT) {
                //Above c
                if( falling && y < c.getY() && Math.abs(y-c.getY())<=CHARACTER_HEIGHT+1) {
                    y = Platform.GROUND - CHARACTER_HEIGHT - 5;
                    repaint();
                    score++;
                    try {
                        c.collapsed();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    c.respawn();
                }
            }
    }
    public void respawn() {

        x = startX;
        y = startY;
        imageView.setFitWidth(CHARACTER_WIDTH);
        imageView.setFitHeight(CHARACTER_HEIGHT);
        isMoveLeft = false;
        isMoveRight = false;
        falling = true;
        canJump = false;
        jumping = false;
    }

    public void collapsed() throws InterruptedException {
        imageView.setFitHeight(5);
        y = Platform.GROUND - 5;
        repaint();
        TimeUnit.MILLISECONDS.sleep(500);
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void trace() {
      
    }

    public KeyCode getLeftKey() {
        return leftKey;
    }

    public KeyCode getRightKey() {
        return rightKey;
    }

    public KeyCode getUpKey() {
        return upKey;
    }
    
    public KeyCode getDashKey() {
    	return dashKey;
    }

    public AnimatedSprite getImageView() { return imageView; }

    public int getScore() {
        return score;
    }

    public double getOffsetX() {
        return offsetX;
    }

    public double getOffsetY() {
        return offsetY;
    }

    public boolean isMoveLeft() {
        return isMoveLeft;
    }

    public boolean isMoveRight() {
        return isMoveRight;
    }


    public boolean isCanJump() {
        return canJump;
    }

    public KeyCode getMuteKey(){return muteKey;}

    public KeyCode getUnmuteKey(){return unmuteKey;}
    
    private void dashAnimate() {
    	try {
       		for(int i=32;i>0;i--) {
       			TimeUnit.MILLISECONDS.sleep(3);
       			imageView.setFitWidth(i);
       		}   		
       		TimeUnit.MILLISECONDS.sleep(5);
       	 imageView.setFitHeight(64);
       	 imageView.setFitWidth(32);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    public int getStamina() {
		return stamina;
	}

	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
}
