package platformer.model;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.transform.Translate;
import javafx.util.Duration;

public  class Animetion extends Group {
    private  Image imageDecline;
    private  Image imagePlay;
    public  final int WIDTH = 60;
    public  final int HEIGHT = 60;
    public  Button playbtn;
    public  Button stopbtn ;
    private  TranslateTransition trisition1;
    private  TranslateTransition trisition2;
    private  FadeTransition fadeIn;
    private  TranslateTransition trisition;
    private  Circle circle;

    private boolean isRun = false;


    public Animetion() {
        playbtn = new Button("Play");
        stopbtn = new Button("Stop");

        Ellipse ellipse = new Ellipse();
        ellipse.setCenterX(75.0f);
        ellipse.setCenterY(35.0f);
        ellipse.setRadiusX(35.0f);
        ellipse.setRadiusY(17.0f);
        Translate translate = new Translate();
        translate.setX(30);
        translate.setY(20);
        ellipse.getTransforms().addAll(translate);
//        ellipse.setLayoutX(30);
//        ellipse.setLayoutY(20);
        ellipse.setFill(Color.WHITE);


        Ellipse ellipse1 = new Ellipse();
        ellipse1.setCenterX(75.0f);
        ellipse1.setCenterY(35.0f);
        ellipse1.setRadiusX(35.0f);
        ellipse1.setRadiusY(17.0f);
        Translate translate1 = new Translate();
        translate1.setX(15);
        translate1.setY(30);
        ellipse1.getTransforms().addAll(translate1);
//        ellipse1.setLayoutX(15);
//        ellipse1.setLayoutY(30);
        ellipse1.setFill(Color.WHITE);


        Ellipse ellipse2 = new Ellipse();
        ellipse2.setCenterX(75.0f);
        ellipse2.setCenterY(35.0f);
        ellipse2.setRadiusX(35.0f);
        ellipse2.setRadiusY(17.0f);
        Translate translate2 = new Translate();
        translate2.setX(45);
        translate2.setY(30);
        ellipse2.getTransforms().addAll(translate2);
        //        ellipse2.setLayoutX(45);
        //        ellipse2.setLayoutY(30);
        ellipse2.setFill(Color.WHITE);

        Group root = new Group(ellipse, ellipse1, ellipse2);
//trisition
        trisition = new TranslateTransition();
        trisition.setDuration(Duration.seconds(3));
        trisition.setToX(100);
        trisition.setNode(root);
        trisition.setAutoReverse(true);
        trisition.setCycleCount(Animation.INDEFINITE);
        trisition.stop();

        //Could 1


        Ellipse ellipse3 = new Ellipse();
        ellipse3.setCenterX(75.0f);
        ellipse3.setCenterY(35.0f);
        ellipse3.setRadiusX(35.0f);
        ellipse3.setRadiusY(17.0f);
        Translate translate3 = new Translate();
        translate3.setX(630);
        translate3.setY(20);
        ellipse3.getTransforms().addAll(translate3);
//        ellipse.setLayoutX(630);
//        ellipse.setLayoutY(20);
        ellipse3.setFill(Color.WHITE);


        Ellipse ellipse4 = new Ellipse();
        ellipse4.setCenterX(75.0f);
        ellipse4.setCenterY(35.0f);
        ellipse4.setRadiusX(35.0f);
        ellipse4.setRadiusY(17.0f);
        Translate translate4 = new Translate();
        translate4.setX(615);
        translate4.setY(30);
        ellipse4.getTransforms().addAll(translate4);
//        ellipse1.setLayoutX(615);
//        ellipse1.setLayoutY(30);
        ellipse4.setFill(Color.WHITE);


        Ellipse ellipse5 = new Ellipse();
        ellipse5.setCenterX(75.0f);
        ellipse5.setCenterY(35.0f);
        ellipse5.setRadiusX(35.0f);
        ellipse5.setRadiusY(17.0f);
        Translate translate5 = new Translate();
        translate5.setX(645);
        translate5.setY(30);
        ellipse5.getTransforms().addAll(translate5);
//        ellipse2.setLayoutX(645);
//        ellipse2.setLayoutY(30;)
        ellipse5.setFill(Color.WHITE);


        Group root1 = new Group(ellipse3, ellipse4, ellipse5);

//Trisition1
        trisition1 = new TranslateTransition();
        trisition1.setDuration(Duration.seconds(3));
        trisition1.setToX(-100);
        trisition1.setNode(root1);
        trisition1.setAutoReverse(true);
        trisition1.setCycleCount(Animation.INDEFINITE);
        trisition1.stop();


        //Cloud2


        circle = new Circle(30);
        circle.setLayoutX(400);
        circle.setCenterY(200);
        circle.setFill(Color.RED);
        circle.setVisible(false);

        trisition2 = new TranslateTransition();
        trisition2.setDuration(Duration.seconds(5));
        trisition2.setToY(-150);
        trisition2.setNode(circle);

        //trisition2.play();

        fadeIn = new FadeTransition(Duration.seconds(3), circle);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        //fadeIn.play();


        playbtn.setTranslateX(10);
        playbtn.setTranslateY(20);
        imagePlay = new Image("platformer/assets/button1.png");
        imageDecline = new Image("platformer/assets/pause.png");
        stopbtn.setTranslateX(90);
        stopbtn.setTranslateY(20);
        playbtn.setGraphic(new ImageView(imagePlay));
        stopbtn.setGraphic(new ImageView(imageDecline));

        playbtn.setStyle("-fx-background-radius: 20em; " + "-fx-font-size: 1.2em; ");
        //  button.setStyle("-fx-font-size: 1.2em; ");
        stopbtn.setStyle("-fx-background-radius: 20em; " + "-fx-font-size: 1.2em; ");

        playbtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                run();
            }

        });

        stopbtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                stop();
//                System.out.p1rint("\n" + "STOP");
            }

        });

        // Group root3 = new Group(root1, root, circle, playbtn, stopbtn);
        this.getChildren().addAll(root1, root, circle, playbtn, stopbtn);
        //return root3;

    }
    public void run(){
        setIsRun(true);
        trisition1.play();
        trisition.play();
        fadeIn.play();
        trisition2.play();
        circle.setVisible(true);

    }

    public void stop(){
        setIsRun(false);
        trisition1.stop();
        trisition.stop();
        fadeIn.stop();
        trisition2.stop();
        circle.setVisible(false);
    }

    public boolean getIsRun() {
        return isRun;
    }

    public void setIsRun(boolean isRun) {
        this.isRun = isRun;
    }
}