package platformer.view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import platformer.model.Animetion;
import platformer.controller.Music;
import platformer.model.Ball;
import platformer.model.Character;
import platformer.model.Keys;
import platformer.model.Score;
import platformer.model.StaminaBar;

import java.util.ArrayList;

public class Platform extends Pane {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    public static final int GROUND = 300;

    private Image platformImg;
    private ArrayList<Character> characterList;
    private ArrayList<Score> scoreList;
    private ArrayList<Ball> pokeball;
    private ArrayList<Score> ball;
    public ArrayList<Score> getBall(){return ball;}
    private Music music;
    private Keys keys;
    private ArrayList<StaminaBar> staminaBarList;
    public Platform() {
        characterList = new ArrayList<>();
        scoreList = new ArrayList();
        staminaBarList = new ArrayList();
        music = new Music();
        music.music();

        pokeball = new ArrayList<>();
        ball = new ArrayList<>();
        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/platformer/assets/Background.png"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        Thread b = new Thread(() -> {
            for (int i = 0 ;i<10;i++){
                int x = (int)Math.floor(Math.random()*800);
                pokeball.add(new Ball(x,10,0,0));
            }
        });
        b.start();
        scoreList.add(new Score(30,GROUND + 30));
        scoreList.add(new Score(Platform.WIDTH-60,GROUND + 30));
        characterList.add(new Character(30, 30,0,0, KeyCode.A,KeyCode.D,KeyCode.W,KeyCode.F));
        characterList.add(new Character(Platform.WIDTH-60, 30,0,96, KeyCode.LEFT,KeyCode.RIGHT,KeyCode.UP,KeyCode.ENTER));
        characterList.add(new Character(KeyCode.M,KeyCode.N));
        ball.add(new Score(120, GROUND + 30));
        ball.add(new Score(Platform.WIDTH - 170, GROUND + 30));
        staminaBarList.add(new StaminaBar(30,GROUND + 70));
        staminaBarList.add(new StaminaBar(Platform.WIDTH-165,GROUND + 70));
        getChildren().add(backgroundImg);
        getChildren().addAll(characterList);
        getChildren().addAll(scoreList);
        getChildren().addAll(new Animetion());
        getChildren().addAll(pokeball);
        getChildren().addAll(ball);
        getChildren().addAll(staminaBarList);
    }

    public ArrayList<Character> getCharacterList() {
        return characterList;
    }

    public Keys getKeys() {
        return keys;
    }

    public Music getMusic(){return music;}

    public ArrayList<Score> getScoreList() {
        return scoreList ;
    }
    public ArrayList<Ball> getPokeball(){
        return pokeball;
    }
    
    public ArrayList<StaminaBar> getstaminaBarList() {
		return staminaBarList;
	}
}

